import java.util.Date;

public class File extends FileSystem implements FileSystemInterface{

    public File(Identification id, Designation name,Date creationDate, Date modificationDate, Size size, Permission permission, Path path){
        super(id, name, creationDate, modificationDate, size, permission, path);
    }


    public void create() {
        //Specific create implementation of File
    }

    public void move() {
        //Specific move implementation of File
    }

    public void copy() {
        //Specific copy implementation of File
    }

    public void update() {
        //Specific update implementation of File
    }
}
