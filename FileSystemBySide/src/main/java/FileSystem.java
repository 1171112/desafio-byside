import java.util.Date;

public abstract class FileSystem {
    private Identification id;
    private Designation name;
    private Date creationDate;
    private Date modificationDate;
    private Size size;
    private Permission permission;
    private Path path;

    public FileSystem(Identification id, Designation name, Date creationDate, Date modificationDate, Size size, Permission permission, Path path){
        this.id=id;
        this.name=name;
        this.creationDate= creationDate;
        this.modificationDate=modificationDate;
        this.size=size;
        this.permission=permission;
        this.path=path;
    }

    public Identification getId() {
        return id;
    }

    public Designation getName(){ return name; }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public Size getSize() {
        return size;
    }

    public Permission getPermission() {
        return permission;
    }

    public Path getPath() {
        return path;
    }


    public abstract void create();

    public abstract void move();

    public abstract void copy();

    public abstract void update();


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileSystem that = (FileSystem) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
