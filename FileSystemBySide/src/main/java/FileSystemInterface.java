public interface FileSystemInterface {

    public void create();

    public void move();

    public void copy();

    public void update();

}
