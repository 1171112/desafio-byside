import java.util.Date;

public class Folder extends FileSystem implements FileSystemInterface{

    public Folder(Identification id, Designation name, Date creationDate, Date modificationDate, Size size, Permission permission, Path path){
        super(id, name, creationDate, modificationDate, size, permission, path);
    }


    public void create() {
        //Specific create implementation of Folder
    }

    public void move() {
        //Specific move implementation of Folder
    }

    public void copy() {
        //Specific copy implementation of Folder
    }

    public void update() {
        //Specific update implementation of Folder
    }

}
