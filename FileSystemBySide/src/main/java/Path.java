
public class Path {
    private String path;

    public Path(String path){
        this.path=path;
    }

    public String getPath(){
        return this.path;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Path path1 = (Path) o;
        return path.equals(path1.path);
    }

    @Override
    public int hashCode() {
        return path.hashCode();
    }
}
