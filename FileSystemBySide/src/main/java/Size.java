
public class Size {
    private double size;

    public Size(double size){
        this.size= size;
    }


    public double getSize() {
        return size;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Size size1 = (Size) o;
        return Double.compare(size1.size, size) == 0;
    }

    @Override
    public int hashCode() {
        return Double.valueOf(size).hashCode();
    }
}
