# Desafio-BySide

A solução apresentada além de recorrer a princípios OO recorre a DDD.

A hierarquia nomalmente é problemática devido ao acoplamento inerente à mesma e prende o projeto a implementações concretas tornando mais dificil a adição de novos requisitos(violando assim o Open-Closed Principle).

Dito isto, optei pela implementação da hierarquia essencialmente devido aos conceitos de domínio serem intimamente relacionados e à dimensão do projeto que não terá adição de requisitos. Penso que é um tradeoff justificado.

Ainda assim, implementei uma interface devido aos seus benificios e também, por permitir, uma eventual alteração na implementação relativamente à hierarquia.
